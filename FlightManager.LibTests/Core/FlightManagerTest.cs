﻿using System;
using System.Collections.Generic;
using FlightManager.Lib.Core;
using FlightManager.Lib.Model;
using NUnit.Framework;

namespace FlightManager.LibTests.Core
{
	[TestFixture]
	internal class FlightManagerTest
	{
		[Test]
		public void SimpleFlightManagerShouldCreateNewFlightInDataStorage()
		{
			Flight flight = new Flight();
			SimpleFlightManager flightManager = new SimpleFlightManager();

			flightManager.Create(flight);

			Assert.That(DataStorage.Flights, Does.Contain(flight));

		}

		[Test]
		public void SimpleFlightManagerShouldDeleteFlightFromDataStorage()
		{
			Flight flight = new Flight();
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				flight
			};

			flightManager.Delete(flight);

			Assert.That(DataStorage.Flights, Does.Not.Contain(flight));
		}

		[Test]
		public void SimpleFlightManagerShouldFindFlightUsingGuid()
		{
			Guid guid = Guid.NewGuid();
			Flight flight = new Flight
			{
				FlightNumber = guid
			};
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				flight,
				new Flight()
			};

			Flight foundFlight = flightManager.Find(guid);

			Assert.That(foundFlight, Is.EqualTo(flight));
		}

		[Test]
		public void SimpleFlightManagerShouldReturnNullWhenFlightWasNotFoundUsingGuid()
		{
			Guid guid = Guid.NewGuid();
			Flight flight = new Flight
			{
				FlightNumber = guid
			};
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				flight,
				new Flight()
			};

			Flight foundFlight = flightManager.Find(new Guid());

			Assert.That(foundFlight, Is.Null);
		}

		[Test]
		public void SimpleFlightManagerShouldFindFlightUsingArrivalPort()
		{
			Flight flight = new Flight
			{
				ArrivalPort = "HEL"
			};
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				flight,
				new Flight()
			};

			Flight foundFlight = flightManager.Find("HEL");

			Assert.That(foundFlight, Is.EqualTo(flight));
		}

		[Test]
		public void SimpleFlightManagerShouldReturnNullWhenFlightWasNotFoundUsingArrivalPort()
		{
			Flight flight = new Flight
			{
				ArrivalPort = "HEL"
			};
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				flight,
				new Flight()
			};

			Flight foundFlight = flightManager.Find("wrong port");

			Assert.That(foundFlight, Is.Null);
		}

		[Test]
		public void SimpleFlightManagerShouldDeleteAllFlights()
		{
			SimpleFlightManager flightManager = new SimpleFlightManager();
			DataStorage.Flights = new List<Flight>
			{
				new Flight(),
				new Flight(),
				new Flight(),
				new Flight(),
				new Flight()
			};

			flightManager.DeleteAll();

			Assert.That(DataStorage.Flights, Is.Empty);
		}
	}
}
