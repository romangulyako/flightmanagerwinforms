﻿using System;
using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System.Collections.Generic;
using System.Linq;
using FlightManager.Lib.Exceptions;

namespace FlightManager.Lib.Core
{
	public class TicketSaleManager : ITicketSaleManager
	{
		public void SellTicket(Flight flight, Passenger passenger, int seatNumber = -1)
		{
			IEnumerable<int> seatsNumbers;

			if (!DataStorage.FlightSeatsCache.TryGetValue(flight.GetHashCode(), out seatsNumbers))
			{
				seatsNumbers = Enumerable.Range(1, flight.SeatsNumber);
				DataStorage.FlightSeatsCache[flight.GetHashCode()] = seatsNumbers;
			}

			if (seatNumber > seatsNumbers.Max())
			{
				throw new ArgumentException("Недопустимый номер места!", nameof(seatNumber));
			}

			if (seatNumber < 0)
			{
				SellAutoAssignedTicket(flight, passenger, seatsNumbers);
			}
			else
			{
				SellManualAssignedTicket(flight, passenger, seatsNumbers, seatNumber);
			}
		}

		public void RefundTicket(Flight flight, Passenger passenger)
		{
			if (DataStorage.ReservedTickets.ContainsKey(flight))
			{
				DataStorage.ReservedTickets[flight]
					.RemoveAll(ticket =>
						ticket.PassengerFirstName == passenger.FirstName &&
						ticket.PassengerLastName == passenger.LastName);
			}
		}

		private Ticket GetTicketForFreeSeatNumber(Passenger passenger, IEnumerable<int> seatsNumbers, List<Ticket> tickets)
		{
			int freeSeatNumber = seatsNumbers.First(number => tickets.All(ticket => ticket.SeatNumber != number));

			return new Ticket
			{
				PassengerFirstName = passenger.FirstName,
				PassengerLastName = passenger.LastName,
				SeatNumber = freeSeatNumber
			};
		}

		private void SellAutoAssignedTicket(Flight flight, Passenger passenger, IEnumerable<int> seatsNumbers)
		{
			List<Ticket> tickets;
			if (DataStorage.ReservedTickets.TryGetValue(flight, out tickets))
			{
				tickets.Add(GetTicketForFreeSeatNumber(passenger, seatsNumbers, tickets));
			}
			else
			{
				tickets = new List<Ticket>();
				tickets.Add(GetTicketForFreeSeatNumber(passenger, seatsNumbers, tickets));
				DataStorage.ReservedTickets[flight] = tickets;
			}
		}

		private void SellManualAssignedTicket(Flight flight, Passenger passenger, IEnumerable<int> seatsNumbers, int seatNumber)
		{
			Ticket ticket = new Ticket
			{
				PassengerFirstName = passenger.FirstName,
				PassengerLastName = passenger.LastName,
				SeatNumber = seatNumber
			};

			List<Ticket> tickets;
			if (DataStorage.ReservedTickets.TryGetValue(flight, out tickets))
			{
				if (tickets.Any(ticket => ticket.SeatNumber == seatNumber))
				{
					throw new SeatIsAssignedException("долрвыаролдваплдро");
				}

				tickets.Add(ticket);
			}
			else
			{
				tickets = new List<Ticket>
				{
					ticket
				};
				DataStorage.ReservedTickets[flight] = tickets;
			}
		}
	}
}
