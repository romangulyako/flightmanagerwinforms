﻿using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System;
using System.Linq;

namespace FlightManager.Lib.Core
{
	public class SimpleFlightManager : IFlightManager
	{
		public void Create(Flight flight)
		{
			DataStorage.Flights.Add(flight);
		}

		public void Delete(Flight flight)
		{
			DataStorage.Flights.Remove(flight);
		}

		public Flight Find(string arrivalPort)
		{
			return DataStorage.Flights.FirstOrDefault(x => x.ArrivalPort == arrivalPort);
		}

		public Flight Find(Guid flightNumber)
		{
			return DataStorage.Flights.FirstOrDefault(x => x.FlightNumber.Equals(flightNumber));
		}

		public void DeleteAll()
		{
			DataStorage.Flights.Clear();
		}
	}
}
