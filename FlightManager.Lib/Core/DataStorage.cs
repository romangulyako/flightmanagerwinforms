﻿using FlightManager.Lib.Model;
using System.Collections.Generic;

namespace FlightManager.Lib.Core
{
	public class DataStorage
	{
		internal static Dictionary<Flight, List<Ticket>> ReservedTickets = new();

		public static List<Flight> Flights = new();

		public static List<Passenger> Passengers = new();

		internal static Dictionary<int, IEnumerable<int>> FlightSeatsCache = new();
	}
}
