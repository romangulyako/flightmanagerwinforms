﻿namespace FlightManager.UI
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.FindByPassport = new System.Windows.Forms.Button();
			this.passportNumberBox = new System.Windows.Forms.TextBox();
			this.passangerLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// FindByPassport
			// 
			this.FindByPassport.Location = new System.Drawing.Point(23, 58);
			this.FindByPassport.Name = "FindByPassport";
			this.FindByPassport.Size = new System.Drawing.Size(112, 34);
			this.FindByPassport.TabIndex = 0;
			this.FindByPassport.Text = "Find";
			this.FindByPassport.UseVisualStyleBackColor = true;
			this.FindByPassport.Click += new System.EventHandler(this.button1_Click);
			// 
			// passportNumberBox
			// 
			this.passportNumberBox.Location = new System.Drawing.Point(12, 12);
			this.passportNumberBox.Name = "passportNumberBox";
			this.passportNumberBox.Size = new System.Drawing.Size(352, 31);
			this.passportNumberBox.TabIndex = 1;
			// 
			// passangerLabel
			// 
			this.passangerLabel.AutoSize = true;
			this.passangerLabel.Location = new System.Drawing.Point(12, 119);
			this.passangerLabel.Name = "passangerLabel";
			this.passangerLabel.Size = new System.Drawing.Size(0, 25);
			this.passangerLabel.TabIndex = 2;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.passangerLabel);
			this.Controls.Add(this.passportNumberBox);
			this.Controls.Add(this.FindByPassport);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button FindByPassport;
		private System.Windows.Forms.TextBox passportNumberBox;
		private System.Windows.Forms.Label passangerLabel;
	}
}
