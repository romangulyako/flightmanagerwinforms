﻿using FlightManager.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlightManager.Lib.Model;

namespace FlightManager.UI
{
	public partial class Form1 : Form
	{
		private readonly PassengerManager _passengerManager = new PassengerManager();
		private readonly SimpleFlightManager _simpleFlightManager = new SimpleFlightManager();

		public Form1()
		{
			InitializeComponent();
			foreach(var passenger in DataFiller.CreatePassengers())
			{
				_passengerManager.Create(passenger);
			}

			foreach (var flight in DataFiller.CreateFlights())
			{
				_simpleFlightManager.Create(flight);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			string passportNumber = passportNumberBox.Text;

			Passenger passenger = _passengerManager.Find(passportNumber);

			if (passenger!=null)
			{
				passangerLabel.Text = passenger.ToString();
			}
			else
			{
				passangerLabel.Text = "Пассажир не найден.";
			}
		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{

		}
	}
}
